package com.example.assignment_1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private val units = listOf<String>(
        "ნული",
        "ერთი",
        "ორი",
        "სამი",
        "ოთხი",
        "ხუთი",
        "ექვსი",
        "შვიდი",
        "რვა",
        "ცხრა",
        "ათი",
        "თერთმეტი",
        "თორმეტი",
        "ცამეტი",
        "თოთხმეტი",
        "თხუთმეტი",
        "თექვსმეტი",
        "ჩვიდმეტი",
        "თვრამეტი",
        "ცხრამეტი"
    )
    private val tens = listOf<String>(
        "",
        "",
        "ოცი",
        "ოცდაათი",
        "ორმოცი",
        "ორმოცდაათი",
        "სამოცი",
        "სამოცდაათი",
        "ოტხმოცი",
        "ოთხმოცდაათი"
    )

    lateinit var number: EditText
    lateinit var convertedNum: TextView
    lateinit var button: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    private fun init() {
        button = findViewById(R.id.convertButton)
        button.setOnClickListener {
            number = findViewById<EditText>(R.id.editTextNumber)
            convertedNum = findViewById<TextView>(R.id.convertedText)
            if (number.text.toString().isEmpty()) {
                convertedNum.text = "შეიყვანე მონაცემი"
            } else {
                try {
                    convertedNum.text = getWord(number.text.toString().toInt())
                } catch (ex: NumberFormatException) {
                    convertedNum.text = "შეიყვანე რიცხვი"
                }
            }

        }
    }

    private fun getBeforeHundred(n: Int): String {
        if (n < 20) return units[n]
        if (n < 100) {
            var digits = if (n % 10 != 0 && (n / 10) % 2 == 0) {
                "${tens[n / 10].dropLast(1)}და${units[n % 10]}"
            } else if (n % 10 != 0 && (n / 10) % 2 != 0) {
                println(units[n % 10 + 10])
                "${tens[n / 10].dropLast(3)}${units[n % 10 + 10]}"
            } else {
                tens[n / 10]
            }
            return digits
        }
        return ""
    }

    private fun getWord(num: Int): String {
        if (num < 100) return getBeforeHundred(num)
        if (num < 1000) {
            return if (num % 100 != 0) {
                val hundred = if (num / 100 == 1) {
                    "ას "
                } else if (num / 100 == 8 || num / 100 == 9) {
                    units[num / 100] + "ას "
                } else {
                    units[num / 100].dropLast(1) + "ას "
                }
                hundred + getBeforeHundred(num % 100)
            } else {
                if (num / 100 != 1) units[num / 100].dropLast(1) + "ასი" else "ასი"
            }

        }
        if (num == 1000) return "ათასი"
        return "მოცემულ დავალებას შეყვანილი მნიშვნელობა არ შეესაბამება"
    }

}